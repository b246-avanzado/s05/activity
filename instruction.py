# Specifications
# 1. Create a Person class that is an abstract class that has the following abstractclassmethods
#   a. getFullName method
#   b. addRequest method
#   c. checkRequest method
#   d. addUser method

# 2. Create an Employee class from Person with the following properties and methods
#   a. Properties(make sure they are private and have getters/setters)
#       firstName, lastName, email, department
#   b. Methods (Don't forget getters and setters)
#   c. Abstract methods
#       i. checkRequest() - placeholder method (For the checkRequest and addUser methods, they should do nothing, for the functions that does nothing, 'pass' keyword will be a great help)
#       ii. addUser  - placeholder method (For the checkRequest and addUser methods, they should do nothing, for the functions that does nothing, 'pass' keyword will be a great help)
#       iii. addRequest - returns "Request has been added"
#       iv. getFullName - returns both object's firstname and lastname through self
#   d. Custom Methods
#       v. login() - returns "<Email> has logged in"
#       vi. logout() - returns "<Email> has logged out"

# 3. Create a TeamLead class from Person with the following properties and methods
#   a. Properties(make sure they are private and have getters/setters)
#       firstName, lastName, email, department, members 
#       Note: members could be a list so it will allow values
#   b. Methods (Don't forget getters and setters)
#       Abstract methods
#       i. addRequest() - For the addRequest() and addUser() methods, they should do nothing. For the functions that do nothing, 'pass' keyword will be a great help)
    #   ii. addUser() - For the addRequest() and addUser() methods, they should do nothing. For the functions that do nothing, 'pass' keyword will be a great help)
#       iii. checkRequest - returns "Request has been checked"
#       iv. getFullName - returns both object's firstname and lastname through self
#   d. Custom Methods
#       v. login() - returns "<Email> has logged in"
#       vi. logout() - returns "<Email> has logged out"
#       Vii. addMember() - adds an employee to the members list


# 4. Create an Admin class from Person with the following properties and methods
#   a. Properties(make sure they are private and have getters/setters)
#       firstName, lastName, email, department
#   b. Methods
#       Abstract methods
#       i. checkRequest() - placeholder method (For the checkRequest() and addRequest() methods, they should do nothing, for the functions that does nothing, 'pass' keyword will be a great help)
#       ii. addRequest() - placeholder method (For the checkRequest() and addRequest() methods, they should do nothing, for the functions that does nothing, 'pass' keyword will be a great help)
#       iii. addUser() - outputs "New user added"
     #  iv. getFullName - returns both object's firstname and lastname through self
#   d. Custom Methods
#       v. login() - outputs "<Email> has logged in"
#       vi. logout() - outputs "<Email> has logged out"
#       Note: All methods just return Strings of simple text
#           ex. Request has been added

# 5. Create a Request that has the following properties and methods
#   a. properties
#       name, requester, dateRequested, status
#   b. Methods (Don't forget to have getters and setters for the properties)
#       i. updateRequest() - returns "Request <name> has been updated"
#       ii. closeRequest() - returns "Request <name> has been closed"
#       iii. cancelRequest() - returns "Request <name> has been cancelled"
#       Note: All methods just return Strings of simple text
#           Ex. Request < name > has been updated/closed/cancelled
