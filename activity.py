from abc import ABC, abstractclassmethod

class Person(ABC):
    @abstractclassmethod
    def getFullName(self):
        pass

    @abstractclassmethod
    def addRequest(self):
        pass

    @abstractclassmethod
    def checkRequest(self):
        pass

    @abstractclassmethod
    def addUser(self):
        pass


class Employee(Person):

    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.department = department

    def set_firstName(self, firstName):
        self.firstName = firstName

    def set_lastName(self, lastName):
        self.lastName = lastName

    def set_email(self, email):
        self.email = email

    def set_department(self, department):
        self.department = department


    def get_firstName(self):
        return(f"Your first name is {self.firstName}")

    def get_lastName(self):
        return(f"Your last name is {self.lastName}")

    def get_email(self):
        return(f"Your email is {self.email}")

    def get_department(self):
        return(f"Your department is {self.department}")

    def checkRequest(self):
        pass

    def addUser(self):
        pass

    def addRequest(self):
        return("Request has been added")

    def getFullName(self):
        return(f"{self.firstName} {self.lastName}")

    def login(self):
        return(f"{self.email} has logged in")

    def logout(self):
        return(f"{self.email} has logged out")

class TeamLead(Person):

    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.department = department
        self.members = []

    def set_firstName(self, firstName):
        self.firstName = firstName

    def set_lastName(self, lastName):
        self.lastName = lastName

    def set_email(self, email):
        self.email = email

    def set_department(self, department):
        self.department = department


    def get_firstName(self):
        return(f"Your first name is {self.firstName}")

    def get_lastName(self):
        return(f"Your last name is {self.lastName}")

    def get_email(self):
        return(f"Your email is {self.email}")

    def get_department(self):
        return(f"Your department is {self.department}")

    def get_members(self):
        return self.members

    def addRequest(self):
        pass

    def addUser(self):
        pass

    def checkRequest(self):
        return("Request has been checked")

    def getFullName(self):
        return(f"{self.firstName} {self.lastName}")
    
    def login(self):
        return(f"{self.email} has logged in")

    def logout(self):
        return(f"{self.email} has logged out")

    def addMember(self, member):
        self.members.append(member)

class Admin(Person):

    def __init__(self, firstName, lastName, email, department):
        super().__init__()
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.department = department

    def set_firstName(self, firstName):
        self.firstName = firstName

    def set_lastName(self, lastName):
        self.lastName = lastName

    def set_email(self, email):
        self.email = email

    def set_department(self, department):
        self.department = department


    def get_firstName(self):
        return(f"Your first name is {self.firstName}")

    def get_lastName(self):
        return(f"Your last name is {self.lastName}")

    def get_email(self):
        return(f"Your email is {self.email}")

    def get_department(self):
        return(f"Your department is {self.department}")

    def addRequest(self):
        pass

    def addUser(self):
        return("New user added")

    def checkRequest(self):
        pass

    def getFullName(self):
        return(f"{self.firstName} {self.lastName}")
    
    def login(self):
        return(f"{self.email} has logged in")

    def logout(self):
        return(f"{self.email} has logged out")
    
class Request():

    def __init__(self, name, requester, dateRequested):
        self.name = name
        self.requester = requester
        self.dateRequested = dateRequested
        self.status = "Pending"

    def set_name(self, name):
        self.name = name
    
    def set_requester(self, requester):
        self.requester = requester

    def set_dateRequested(self, dateRequested):
        self.dateRequested = dateRequested

    def set_status(self, status):
        self.status = status

    def get_name(self):
        return(f"This request's name is {self.name}")

    def get_requester(self):
        return(f"This request is requested by {self.requester}")

    def get_dateRequested(self):
        return(f"This request was made on {self.dateRequested}")

    def get_status(self_):
        return(f"This request is {self.status}")

    def updateRequest(self):
        return(f"Request {self.name} has been updated")

    def closeRequest(self):
        return(f"Request {self.name} has been closed")

    def cancelRequest(self):
        return(f"Request {self.name} has been cancelled")
    
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)
for indiv_emp in teamLead1.get_members():
    print(indiv_emp.getFullName())

# assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())
